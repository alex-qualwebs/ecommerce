<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{  
	use HasApiTokens;


	protected $guard = 'admin';

     protected $fillable = [
        'first_name', 'last_name', 'email', 'mobile_number', 'password','last_login_time','last_login_ip','status'
    ];

 }



