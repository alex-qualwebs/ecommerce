<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Passwords\createToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;

class UserController extends Controller
{   
    public function register(Request $request)
    {  

       $validator = Validator::make($request->all(),[

          'first_name' => 'required|alpha',
          'last_name'  => 'required|alpha',
          'email'      => 'required|email|unique:users',
          'mobile_number' => 'required|numeric|digits:10|unique:users',
          'password'    => 'required|min:6',
          'cpassword'   => 'required|same:password'
  
       ]); 
       
       if($validator->fails())
       {
         return response()->json(['error'=> $validator->errors()], 401);
       }

        $input = $request->all();
        $input['password']= bcrypt($input['password']);
       
        //user registration
        $user = User::create($input);
        
        //genrate token
        $success['token'] = $user->createToken('Myapp')->accessToken;
        $success['message']= "you are registerd succesfully";

        return response()->json(['success'=>$success], 200);

     }

     public function login(Request $request)
     {   
      $validator = Validator::make($request->all(),[

        'email'      => 'required|email',
        'password'    => 'required',

      ]); 

     if($validator->fails())
     {
       return response()->json($validator->errors());
     }
     else
     {

        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
        { 
       
        auth()->user()->update([
            'last_login_time' => Carbon::now()->toDateTimeString(),
            'last_login_ip' => request()->getClientIp(),
            'status'        => 1,
          ]);
       
           $user = Auth::user();
           $success['token'] = $user->createToken('Myapp')->accessToken;
           return response()->json(['success'=>$success],200);
         }
       else
         {  
          return response()->json(['error'=>"email and password not match"],401);
         }

      }

}

   public function logout()
   {  
     //delete token for authenticated user
      Auth()->user()->token()->revoke();
      
      //offline
      auth()->user()->update([

          'status'  => 0,
      ]);
     //destroy all the session & logout
      Session::flush();

      return response()->json(['msg'=>"logged out"],200);
   }


}
