<?php

namespace App;

use App\Address_type;
use App\City;
use App\Country;
use App\State;
use Illuminate\Database\Eloquent\Model;

class User_address extends Model
{
    protected $fillable = ['address_line_1','address_line_2','user_id','state_id','country_id','city_id','address_type_id'];

   
  public function Country()
   {
      return $this->belongsTo(Country::class,'country_id','id');
   }

   public function State()
   {
      return $this->belongsTo(State::class,'state_id','id');
   }

   public function City()
   {
      return $this->belongsTo(City::class,'city_id','id');
   }

   public function Type()
   {
      return $this->belongsTo(Address_type::class,'address_type_id','id');
   }


}
