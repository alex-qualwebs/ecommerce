<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register','API\UserController@register');
Route::post('login','API\UserController@login');
Route::get('logout','API\UserController@logout')->middleware('auth:api');


Route::resource('address','API\User_address_controller')->middleware('auth:api');

Route::post('admin/register','API\AdminController@register');
Route::post('admin/login','API\AdminController@login');
Route::get('admin/logout','API\AdminController@logout')->middleware('auth:api-admin');
