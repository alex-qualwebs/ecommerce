<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address_type extends Model
{
    protected $guarded = [];

    public function Address()
   {
      return $this->hasOne(User_address::class);
   }
}
