<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use App\User_address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;


class User_address_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $user_id = Auth::user()->id;
       // $data = User_address::where(['user_id'=>$user_id])->get();
       
       $user = User::find($user_id);
       
       return response()->json($user->Address);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 

        $validator = Validator::make($request->all(),[

          'address_line_1'  => 'required',
          'address_line_2'  => 'required',
          'state_id'        => 'required|numeric',
          'city_id'         => 'required|numeric',
          'country_id'      => 'required|numeric',
          'address_type_id' => 'required|numeric|between:1,2'
  
       ]); 

        if($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()]);
        }
        else
        { 
                $id = Auth::user()->id;
                $address = $request->all();
                $address['user_id']= $id;
                $user = User_address::create($address);
                return response()->json(['success'=>"Address stored"],200);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
   
  $user_id = Auth::user()->id;
  $data = User_address::where(['user_id'=>$user_id,'address_type_id'=>$id])->first();
    
    if($data)
       {  
            $User_address_id = $data->id;
            $address = User_address::find($User_address_id);
            $city = $address->City->city_title;
            $state = $address->State->state_title;
            $type = $address->Type->address_type;
            $country =$address->Country->country_title;
            $add1=$address->address_line_1;
            $add2=$address->address_line_2;

             return response()->json([
            'Address_line_1' =>$add1,
            'Address_line_2' =>$add2,
            'City'           =>$city,
            'State'          =>$state,
            'Country'        =>$country,
            'Address_Type'   =>$type,
            'other_data'     =>$data,

        ]);
       }
       else
       {
         return response()->json(['error'=>"Addresh_type_id not valide"]);
       }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
    $user_id = Auth::user()->id;
    $data = User_address::where(['user_id'=>$user_id,'address_type_id'=>$id])->get();

       if($data)
       {
          return response()->json(['addresses' => $data]);
       }
       else
       {
         return response()->json(['error'=>"Addresh_type_id not found"]);
       }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //hear $id referce to user_address_id
       $address=User_address::find($id);

        if($address)
        {
            $address->update($request->all());

            return response()->json([$address]);
        }
        else
        {
            return response()->json(['error'=>"User_address_id not found"]);
        } 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User_address::find($id);

      if($user)
      {
          $user->delete();
          return response()->json(['success'=>"address deleted succesfully"],200);
      }
      else
      {
          return response()->json(['error'=>"User_address_id not found"]);
      }
    }
}
