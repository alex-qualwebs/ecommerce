<?php

namespace App\Http\Controllers\API;

use App\Admin;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;

class AdminController extends Controller
{
    public function register(Request $request)
    {
    	$validator = Validator::make($request->all(),[

         'first_name' => 'required|alpha',
          'last_name'  => 'required|alpha',
          'email'      => 'required|email|unique:admins',
          'mobile_number' => 'required|numeric|digits:10|unique:admins',
          'password'    => 'required|min:6',
          'cpassword'   => 'required|same:password'

    	]);


    	if($validator->fails())
    	{
    		return response()->json(['errors'=> $validator->errors()]);
    	}
    	else
    	{
    		$input = $request->all();
    		$input['password'] = bcrypt($input['password']);

    		$admin =  Admin::create($input);

    		$success['token'] = $admin->createToken('Myapp')->accessToken;
            $success['message']= "you are registerd succesfully" ;

            return response()->json(['success'=>$success], 200);
    	}	
    }


    public function login(Request $request)
    {
    	$validator = Validator::make($request->all(),[

               'email' => 'required|email',
               'password' => 'required'
    	]);

    	if($validator->fails())
    	{
    		return response()->json(['error'=>$validator->errors()]);
    	}
    	else
    	{ 
    		$admin = Auth::guard('admin')->attempt($request->only('email','password'));

    	   if($admin)
    	   {   
                   Auth::guard('admin')->user()->update([
		            'last_login_time' => Carbon::now()->toDateTimeString(),
		            'last_login_ip' => request()->getClientIp(),
		            'status'        => 1,
		          ]);

                   $admin = Auth::guard('admin')->user();

                   $success['token'] = $admin->createToken('Myapp')->accessToken;
                   $success['msg'] = "you are login";

               return response()->json(['success'=>$success],200);
    	   }
    	   else
    	   {
    	   	 return response()->json(['error'=>"email and password not match"]);
    	   }
    	}	
    }

    public function logout()
    {

      	//delete token for authenticated user
      Auth::guard('api-admin')->user()->token()->revoke();
      
      //offline
      Auth::guard('api-admin')->user()->update([

          'status'  => 0,
      ]);
     //destroy all the session & logout
      Session::flush();

      return response()->json(['msg'=>"you are logout"],200);
    }
}
